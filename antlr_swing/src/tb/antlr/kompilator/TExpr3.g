tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer count = 0;
}
prog    : (e+=expr | e+=condition | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje><name;separator=\" \n\"> ";

decl  : ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dec(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}
    
condition : ^(EQUAL e1=expr e2=expr) -> is_equal(p1={$e1.st},p2={$e2.st})
          | ^(NOTEQUAL e1=expr e2=expr) -> is_not_equal(p1={$e1.st},p2={$e2.st})
          ;

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> set(n={$i1.text},p={$e2.st})
        | ID -> get(id={$ID.text})
        | ^(IF cond=condition yes=expr no=expr) {count++;} -> if_statement(cond={$cond.st}, yes={$yes.st}, no={$no.st}, count={count.toString()})
        | INT  {numer++;}          -> int(i={$INT.text})
    ;
    