tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (e=expr {drukuj ($e.text + " = " + $e.out.toString());} | decl)* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ^(IF e1=expr e2=expr e3=expr) { }
        | ID                       {$out = getVar($ID.text);}
        ;

decl
  : ^(VAR id=ID) { declare($id.text);}
  | ^(PODST id=ID  e2=expr) {assign($id.text, $e2.out);}
  ;