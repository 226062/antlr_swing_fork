package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	private GlobalSymbols globals = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) { 
		return Integer.parseInt(text);
	}
	
	protected Integer divide(Integer a, Integer b) {
		if (b == 0) {
			throw new RuntimeException("Can't divide by 0!");
		}
		return a / b;
	}
	
	protected void declare(String name) {
		System.out.println("Declaring variable " + name);
		globals.newSymbol(name);
	}
	
	protected void assign(String name, Integer value) {
		System.out.println("Setting variable " + name + " = " + value);
		if (!globals.hasSymbol(name)) {
			throw new RuntimeException("Variable wasn't declared!");
		}
		globals.setSymbol(name, value);
	}
	
	protected Integer getVar(String name) {
		System.out.println("Getting variable " + name);
		if (!globals.hasSymbol(name)) {
			throw new RuntimeException("Variable wasn't declared!");
		}
		return globals.getSymbol(name);
	}
	
}
