package tb.antlr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.junit.Test;

import tb.antlr.ExprParser.prog_return;
import tb.antlr.interpreter.TExpr1;

public class TestInterpreter {
	
	String interpretInput(String txt) {
		UserInputParser parser = new UserInputParser();
		
		ANTLRStringStream stream = parser.parse(txt);
		ExprLexer lexer = parser.parse(stream);
		CommonTokenStream tokens = parser.parse(lexer);
		prog_return root = parser.parse(tokens);
		
		CommonTreeNodeStream nodes = new CommonTreeNodeStream(root.tree);
        nodes.setTokenStream(tokens);
        
        TExpr1 walker = new TExpr1(nodes);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream old = System.out;
		System.setOut(new PrintStream(baos));

		try {
			walker.prog();
		} catch (RecognitionException e) {
			e.printStackTrace();
		}
		
		System.setOut(old);

		return baos.toString();
	}

	@Test
	public void testAddition() {
		String output = interpretInput("1 + 2\n");
		System.out.println(output);
		assertEquals("1 + 2  = 3\n", output);
	}
	
	@Test
	public void testSubtraction() {
		String output = interpretInput("1 - 2\n");
		System.out.println(output);
		assertEquals("1 - 2  = -1\n", output);
	}
	
	@Test
	public void testMultiplication() {
		String output = interpretInput("1 * 2\n");
		System.out.println(output);
		assertEquals("1 * 2  = 2\n", output);
	}
	
	@Test
	public void testDivision() {
		String output = interpretInput("1 / 2\n");
		System.out.println(output);
		assertEquals("1 / 2  = 0\n", output);
		
		try {
			output = interpretInput("1 / 0\n");
			fail();
		} catch (Exception e) {
			System.out.println(e + output);
		}
	}
	
	@Test
	public void testDeclaration() {
		String output = interpretInput("var x\n");
		System.out.println(output);
		assertEquals("Declaring variable x\n", output);
	}
	
	@Test
	public void testAssignment() {
		String output = interpretInput("var x\nx=2\n");
		System.out.println(output);
		assertEquals("Declaring variable x\n" + 
				"Setting variable x = 2\n", output);
	}
}

