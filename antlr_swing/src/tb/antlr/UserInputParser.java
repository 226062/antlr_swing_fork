package tb.antlr;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTreeNodeStream;

public class UserInputParser {
	public ANTLRStringStream parse(String text) {
		return new ANTLRStringStream(text);
	}
	
	public ExprLexer parse(ANTLRStringStream stringStream) {
		return new ExprLexer(stringStream);
	}
	
	public CommonTokenStream parse(ExprLexer lexer) {
		return new CommonTokenStream(lexer);
	}
	
	public ExprParser.prog_return parse(CommonTokenStream tokens) {
		
		ExprParser parser = new ExprParser(tokens);

		ExprParser.prog_return root = null;
		try {
			root = parser.prog();
		} catch (RecognitionException e) {
			e.printStackTrace();
		}
		return root;
	}
}
