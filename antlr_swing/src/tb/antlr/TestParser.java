package tb.antlr;

import org.junit.Test;

import tb.antlr.ExprLexer;
import tb.antlr.UserInputParser;
import tb.antlr.ExprParser.prog_return;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

public class TestParser {
	
	public String parseInput(String txt) {
		UserInputParser parser = new UserInputParser();
		
		ANTLRStringStream stream = parser.parse(txt);
		ExprLexer lexer = parser.parse(stream);
		CommonTokenStream tokens = parser.parse(lexer);
		prog_return root = parser.parse(tokens);
		 
	    return root.tree.toStringTree();
	}

	@Test
	public void testAddition() {
		String output = parseInput("1 + 2\n");
	    System.out.println(output);   
		assertEquals(output, "(+ 1 2)");
	}
	
	@Test
	public void testSubtraction() {
		String output = parseInput("1 - 2\n");
	    System.out.println(output);
	    assertEquals(output, "(- 1 2)");
	}
	
	@Test
	public void testMultiplication() {
		String output = parseInput("1 * 2\n");
	    System.out.println(output);
	    assertEquals(output, "(* 1 2)");
	}

	
	@Test
	public void testDivision() {
		String output = parseInput("1 / 2\n");
	    System.out.println(output);
	    assertEquals(output, "(/ 1 2)");
	}


}

