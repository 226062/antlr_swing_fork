package tb.antlr;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.io.IOException;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateGroup;
import org.junit.Test;

import tb.antlr.ExprParser.prog_return;
import tb.antlr.kompilator.TExpr3;

public class TestCompiler {
	
	public String getCompilerOutput(String txt) {
		UserInputParser parser = new UserInputParser();
		
		ANTLRStringStream stream = parser.parse(txt);
		ExprLexer lexer = parser.parse(stream);
		CommonTokenStream tokens = parser.parse(lexer);
		prog_return root = parser.parse(tokens);
		
		CommonTreeNodeStream nodes = new CommonTreeNodeStream(root.tree);
		String packagePath = TExpr3.class.getPackage().getName().replaceAll("\\.", "/");
		FileReader groupFile;
		StringTemplateGroup templates = null;
		try {
			groupFile = new FileReader("src/" + packagePath + "/pierwszy.stg");
			templates = new StringTemplateGroup(groupFile);
			groupFile.close();
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}

		TExpr3 walker = new TExpr3(nodes);
		walker.setTemplateLib(templates);
		TExpr3.prog_return tpl = null;
		try {
			tpl = walker.prog();
		} catch (RecognitionException e) {
			e.printStackTrace();
		}

		StringTemplate stp = (StringTemplate) tpl.getTemplate();

		return stp.toString();
	}

	@Test
	public void testAddition() {
		String output = getCompilerOutput("1 + 4\n");
		System.out.println(output);
		assertEquals(output, "MOV A,#4\n" + 
							 "PUSH A\n" + 
							 "MOV A,#1\n" + 
							 "POP B\n" + 
							 "ADD A,B ");
	}
	
	@Test
	public void testSubtraction() {
		String output = getCompilerOutput("7 - 5\n");
		System.out.println(output);
		assertEquals(output, "MOV A,#5\n" + 
							 "PUSH A\n" + 
							 "MOV A,#7\n" + 
							 "POP B\n" + 
							 "SUB A,B ");
	}
	
	@Test
	public void testMultiplication() {
		String output = getCompilerOutput("1 * 2\n");
		System.out.println(output);
		assertEquals(output, "MOV A,#2\n" + 
							 "PUSH A\n" + 
							 "MOV A,#1\n" + 
							 "POP B\n" + 
							 "MUL A,B ");
	}
	
	@Test
	public void testDivision() {
		String output = getCompilerOutput("1 / 2\n");
		System.out.println(output);
		assertEquals(output, "MOV A,#2\n" + 
							 "PUSH A\n" + 
							 "MOV A,#1\n" + 
							 "POP B\n" + 
							 "DIV A,B ");
	}
	
	@Test
	public void testDeclaration() {
		String output = getCompilerOutput("var x\n");
		System.out.println(output);
		assertEquals(output, "DD x\n ");
	}
	
	@Test
	public void testAssignment() {
		String output = getCompilerOutput("var x=8\n");
		System.out.println(output);
		assertEquals(output, "DD x\n"+
							 "MOV A,#8\n"+
							 "MOV [x],A ");
	}
}
